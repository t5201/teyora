import UserAuth from "../../../helpers/UserAuth";
import APIRouter from "../../APIRouter";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: true, // No eligibility for this, used in First Time Setup
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { req, res, db, CRI } = r;

            // If no WikiID
            if (!req.query.wikiID) {
                res.status(400).json({
                    error: "Bad Request",
                    message: "No wikiID provided"
                });
                return;
            }

            try {
                const user = await UserAuth.unlockUser(db, APIRouter.wmfOAuth, CRI, req.query.wikiID.toString());
                res.status(200).json(user);
            } catch (error) {
                // An issue occured with the wiki
                res.status(500).json({
                    message: "Failed to unlock user on wiki",
                    error: error.message
                });
            }
        },
    });
    route.run(props.req, props.res);
};