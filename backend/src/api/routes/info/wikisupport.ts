import { WikiSupport } from "../../../info/wikis/WikiSupport";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: true, // No eligibility for this, used in First Time Setup
        },
        okayRoute: async (r: AuthedRouteExit) => {
            r.res.status(200).json(WikiSupport);
        },
    });
    route.run(props.req, props.res);
};