import APIRouter from "../../APIRouter";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const { res } = props;
    try {
        const authURL = await APIRouter.wmfOAuth.getAuthURL();
        // Store the client secret as a cookie
        res.cookie("TEMPOAuthSecret", authURL.oauthTokenSecret);
        res.cookie("TEMPOAuthToken", authURL.oauthToken);

        // Record the value of the get param "after" if present
        // This handles things like redirects etc.
        if (props.req.query.after) res.cookie("TEMPOAuthAfter", props.req.query.after);
        
        res.redirect(authURL.authURL);
    } catch (error) {
        res.status(500).json({
            message: "Something went wrong",
            error: error.message
        });
    }
};