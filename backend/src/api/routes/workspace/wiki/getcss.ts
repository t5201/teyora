import axios from "axios";
import { WorkspaceEntry } from "../../../../db/dbcollections/workspace";
import WorkspaceHelper from "../../../../helpers/WorkspaceHelper";
import { WikiSupport } from "../../../../info/wikis/WikiSupport";
import APIRouter from "../../../APIRouter";
import AuthedRoute, { AuthedRouteExit } from "../../../AuthedRoute";
import { RouteParams } from "../../RouteDefs";

/**
 * Get Wiki CSS
 * Grabs the stylesheets from each Wiki required to render a page
 */


export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { db, CRI } = r;
            const { workspaceID } = r.req.query;
            if (!workspaceID) {
                r.res.status(400).json({
                    error: "Missing one of the following: workspaceID",
                });
                return;
            }

            // Limit the workspace ID length to prevent DoS
            if (workspaceID.length > 24) {
                r.res.status(403).json({
                    error: "Operation not permitted",
                });
                return;
            }

            try {

                // Let's grab the workspace
                const workspace: WorkspaceEntry = await WorkspaceHelper.getWorkspace(
                    db,
                    workspaceID as string,
                    CRI.user.userID
                );

                if (workspace.workspaceIsLocked) {
                    r.res.status(403).json({
                        error: "Workspace is locked",
                    });
                    return;
                }

                // Okay, now let's grab all the Wiki IDs from the workspace configuration
                const wikiIDs: string[] = workspace.workspaceConfig.wikis;

                // If wikiIDs is empty, we can't do anything
                if (!wikiIDs || wikiIDs.length === 0) {
                    r.res.status(200).json({});
                    return;
                }

                // Enforce the limit of max 15 wikis to each workspace
                if (wikiIDs.length > 15) {
                    r.res.status(400).json({
                        error: "Too many wikis in workspace",
                    });
                    return;
                }
                // wikiID: [urls]
                const cssResults: Record<string, string[]> = {};

                // Okay, now let's grab all the Wiki IDs from the workspace configuration
                // and build a promise queue
                const queue: Promise<void>[] = wikiIDs.map(wikiID => {
                    return (async () => {
                        // Get the WikiInfo for the wiki
                        const wikiInfo = WikiSupport[wikiID];
                        if (!wikiInfo) {
                            // We obviously can't run on a wiki we have no record of
                            return;
                        }

                        // Make a request to get the HTML from Special:Version as this should contain just some CSS
                        // We also require MediaWiki
                        const wikiPageRequest = (await axios.get(`${wikiInfo.url}/wiki/Special:Version`)).data;
                        
                        // Get all the URLs from the stylesheet link tags in the HTML string
                        const cssURLs = wikiPageRequest.match(/<link rel="stylesheet" href="(.*?)"/g) || [];
                        cssResults[wikiID] = cssURLs.map((cssURL: string) => {
                            let rs = cssURL.match(/<link rel="stylesheet" href="(.*?)"/)[1];
                            // If the URL starts with a slash append the base URL to it
                            if (rs.startsWith("/")) rs = `${wikiInfo.url}${rs}`;
                            return rs;
                        });

                    })();
                });

                // Wait for all the promises to resolve
                await Promise.all(queue);

                r.res.status(200).json({
                    cssResults,
                });
            } catch (error) {
                r.res.status(400).json({
                    error: "Workspace not found, operation not permitted or other error",
                });
            }
        },
    });
    route.run(props.req, props.res);
};