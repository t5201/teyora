import Nano from "nano";
export interface iAdminLogEntry extends Nano.MaybeDocument {
    teyoraActionID: string, // The ID of the action, used to identify the action
    username: string, // The username of the admin
    userID: string, // The userID of the admin who completed the action
    action: string, // The action that was completed
    actionSource: string, // The source of the action, e.g. Teyora Website or manually added
    actionDate: Date, // The date the action was completed
    actionTarget: string, // The target of the action, e.g. the username of the user who was affected
    actionDetails: any, // The details of the action, e.g. the user that was added/removed and why
    isPublic: boolean // Whether the action is public or not, only admins can change this
}

export class AdminLogEntry implements iAdminLogEntry {
    _id?: string;
    _rev?: string;
    teyoraActionID: string;
    username: string;
    userID: string;
    action: string;
    actionSource: string;
    actionDate: Date;
    actionTarget: string;
    actionDetails: any;
    isPublic: boolean;
    
    constructor(
        teyoraActionID: string,
        username: string,
        userID: string,
        action: string,
        actionSource: string,
        actionDate: Date,
        actionTarget: string,
        actionDetails: any,
        isPublic: boolean
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.teyoraActionID = teyoraActionID;
        this.username = username;
        this.userID = userID;
        this.action = action;
        this.actionSource = actionSource;
        this.actionDate = actionDate;
        this.actionTarget = actionTarget;
        this.actionDetails = actionDetails;
        this.isPublic = isPublic;
    }

    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}