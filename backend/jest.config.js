module.exports = {

    transform: {
        "^.+\\.tsx?$": "ts-jest",
    },
    testRegex: "(/tests/)(.*?)(\\.tsx?|\\.jsx?)$",
    moduleFileExtensions: ["ts", "tsx", "js"]

};