// Handles links to the app
// link format: root.org/#/namespace/param1/param2/param3

export default class LinkHandler {
    public urlParts: string[];
    public namespace: string;
    public params: string[];

    constructor() {
        // Set up the page to reload when the hash changes
        window.addEventListener("hashchange", () => {
            window.location.reload();
        }
        );
    }

    public parseCurrentURL() : LinkHandler {
        this.urlParts = window.location.hash.split("/");
        if (this.urlParts.length < 2) {
            this.namespace = "";
            this.params = [];
            return this;
        }

        this.urlParts.shift();

        this.namespace = this.urlParts[0];

        this.params = this.urlParts.length > 1 ? this.urlParts.slice(1) : [];

        return this;
    }

    public setURL(namespace: string, params: string[]) : void {
        window.location.hash = `#/${namespace}/${params.join("/")}`;
        this.parseCurrentURL();
    }
}