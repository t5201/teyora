// This is Teyora's main screen/UI - it needs a workspace to work with
// which it will load its configuration from.

import { Alert, Box, Button, Drawer, Stack, Toolbar, Typography } from "@mui/material";
import * as React from "react";
import { ErrorBoundary } from "react-error-boundary";
import Teyora from "../../../App";
import i18n from "../../../i18n";
import { LocalisedButtons } from "../../LocalisedButtons";
import TeyoraUI from "../../TeyoraUI";
import RenderTab, { createTabID, TabElement } from "./baseclasses/RenderTab";
import RenderWikiPage from "./content/wikipage/RenderWikiPage";
import FeedDrawerContent from "./feeddrawer/DrawerContent";
import Homepage from "./content/Homepage";
import JobsDrawerContent from "./jobsdrawer/DrawerContent";
import PatrolLoading from "./Loading";
import PatrolAppBar from "./PatrolAppBar";
import WorkspaceSettings from "./settings/WorkspaceSettings";
import SupaSearch from "./supasearch/SupaSearch";
import WikiPageInfo from "./content/wikipage/WikiPageInfo";
import { BrokenImage } from "@mui/icons-material";

export interface TeyoraPatrolProps { // Only do specific stuff, the rest is handled by the WorkspaceManager
    beforeLoad?: () => void; // Not used yet
}

export interface ContentTab {
    id: string;
    title: string;
    subtitle: string;
    // eslint-disable-next-line no-unused-vars
    content: TabElement;
    actionDrawerContent?: TabElement;
    topControls?: TabElement; // todo
    // eslint-disable-next-line no-unused-vars
    onSwitch?: (allowSwitch: () => void) => void;
    // eslint-disable-next-line no-unused-vars
    onClose?: (allowClose: () => void) => void;
    openFeedDrawer?: boolean;
    openActionDrawer?: boolean;
}

interface TeyoraPatrolState {
    // Again, only specifics, leave complex things to workspace manager, or *importantly* individual
    // components.
    isReady: boolean;
    feedDrawerOpen: boolean; // Left
    actionDrawerOpen: boolean; // Right
    jobsDrawerOpen: boolean; // Bottom
    jobsDrawerFooterMode: boolean; // Bottom drawer, whether it's a footer or a drawer
    currentTabs: ContentTab[];
    currentTabIndex: number;
    supaSearchOpen: boolean;
}

export class TeyoraPatrol extends React.Component<TeyoraPatrolProps, TeyoraPatrolState> {
    static TP: TeyoraPatrol;

    constructor(props: TeyoraPatrolProps) {
        const { t } = i18n;
        super(props);
        this.state = {
            isReady: false,
            feedDrawerOpen: true,
            actionDrawerOpen: true,
            jobsDrawerOpen: true,
            jobsDrawerFooterMode: true,
            currentTabs: [
                {
                    id: createTabID(),
                    title: t("patrol:home.tabTitle"),
                    subtitle: t("patrol:home.tabSubtitle"),
                    content: (tm) => { return <Homepage {...tm} />; },
                }
            ],
            currentTabIndex: 0,
            supaSearchOpen: false,
        };

        TeyoraPatrol.TP = this; // For use in other components
    }

    public toggleFeedDrawer() {
        this.setState({
            feedDrawerOpen: !this.state.feedDrawerOpen,
        });
    }

    public toggleActionDrawer() {
        this.setState({
            actionDrawerOpen: !this.state.actionDrawerOpen,
        });
    }

    public toggleJobsDrawer() {
        this.setState({
            jobsDrawerOpen: !this.state.jobsDrawerOpen,
        });
    }

    public openSupaSearch() {
        this.setState({
            supaSearchOpen: true,
        });
    }

    public ready() {
        this.setState({
            isReady: true,
        });
    }

    public toggleJobsDrawerFooterMode() {
        this.setState({
            jobsDrawerFooterMode: !this.state.jobsDrawerFooterMode,
        });
    }

    public newTabOpenerAnimation() {
        // This is set to a handler in the appbar which shows a fancy animation when a tab is opened
        console.log("No new tab animation set");
    }


    public addTab(tab: ContentTab, replaceCurrentTab = false, callback?: () => void) {
        const { t } = i18n;
        const { currentTabs } = this.state;
        const nextStep = async () => {
            if (replaceCurrentTab && this.state.currentTabIndex !== 0) { // Replace current tab is ignored on homepage
                // Dispose of the state
                try {
                    this.__tabPStateDisposals[this.state.currentTabs[this.state.currentTabIndex].id]();
                } catch (error) {
                    // Somebody has been silly and forgot to add a disposal route for this tab
                    console.error(error);
                    // This can cause memory leaks, so warn the user
                    TeyoraUI.showSnackbar(t("patrol:home.tabDisposeError"), {
                        variant: "warning",
                    });
                }
                // Replace
                currentTabs[this.state.currentTabIndex] = tab;
                // Update the state
                this.setState({
                    currentTabs,
                    currentTabIndex: this.state.currentTabIndex,
                });
                callback();
            } else {
                this.newTabOpenerAnimation();

                // Wait for animation (250ms)
                await new Promise((resolve) => setTimeout(resolve, 250));
                currentTabs.push(tab);
                if (currentTabs.length > 50) TeyoraUI.showAlertBox(t("patrol:home.tooManyTabs.message"), t("patrol:home.tooManyTabs.title"));
                this.setTabIndex(currentTabs.length - 1, callback);
            }
        };

        if (currentTabs[this.state.currentTabIndex].onClose && replaceCurrentTab) {
            currentTabs[this.state.currentTabIndex].onClose(nextStep);
        } else {
            nextStep();
        }
    }

    // Array that holds the state disposal functions for each tab ID
    // Internal use only
    public __tabPStateDisposals: { [key: string]: () => void } = {};

    public closeTab(index: number, callback?: () => void) {
        const { t } = i18n;
        if (index === 0) return; // Don't close the homepage
        const nextStep = async () => {
            // Dispose of the state
            try {
                this.__tabPStateDisposals[this.state.currentTabs[this.state.currentTabIndex].id]();
            } catch (error) {
                // Somebody has been silly and forgot to add a disposal route for this tab
                console.error(error);
                // This can cause memory leaks, so warn the user
                TeyoraUI.showAlertBox(t("patrol:home.tabDisposeError"));
            }

            // Remove the tab from the array
            currentTabs.splice(index, 1);
            // Recalculate the current tab index.
            // If close tab is the current tab, switch to the previous tab (if present)
            // Else, if tab index is lower than the current tab index, subtract 1
            let newTabIndex = this.state.currentTabIndex;
            if (index === this.state.currentTabIndex) {
                newTabIndex--;
            }

            if (this.state.currentTabIndex > index) {
                newTabIndex--;
            } else if (this.state.currentTabIndex < index) {
                newTabIndex++;
            }

            this.setState({
                currentTabs,
                currentTabIndex: newTabIndex,
            });
            callback();
        };

        const { currentTabs } = this.state;
        if (currentTabs[index].onClose) {
            currentTabs[index].onClose(nextStep);
        } else {
            nextStep();
        }

    }

    public setTabIndex(index: number, callback?: () => void) {
        if (this.state.currentTabs[index].onSwitch) {
            this.state.currentTabs[index].onSwitch(() => {
                this.setState({
                    currentTabIndex: index,
                });
                if (callback) callback();
            });
        } else {
            this.setState({
                currentTabIndex: index,
            });
        }
    }

    public openWorkspaceSettings() {
        const { t } = i18n;
        this.addTab({
            id: createTabID(),
            title: t("patrol:settings.tabTitle"),
            subtitle: t("patrol:settings.tabSubtitle"),
            content: (tm) => <WorkspaceSettings {...tm} />, //todo
        });
    }

    // Open a Wiki page
    public openWikiPage(wikiID: string, pageTitle: string, openInNewTab: boolean) {
        // Get Wikisupport for this wiki
        const wikiSupport = Teyora.TY.WikiSupport[wikiID];

        this.addTab({
            id: createTabID(),
            title: pageTitle,
            // Wiki name here, we should probably extract this into its own format func
            subtitle: ((wikiSupport.langName !== "Special") && wikiSupport.langName + " ") + wikiSupport.name,
            content: (tm) => { return <RenderWikiPage {...tm} wikiID={wikiID} pageTitle={pageTitle} />; },
            actionDrawerContent: (tm) => { return <WikiPageInfo {...tm} wikiID={wikiID} pageTitle={pageTitle} />; },
        }, !openInNewTab);
    }

    render() {
        const lB = LocalisedButtons();
        if (!this.state.isReady) return (<PatrolLoading />);

        return <Box
            sx={{
                margin: 0,
                padding: 0,
                bgcolor: "background.default",
                color: "text.primary",
                height: "100%",
                overflow: "hidden", // Prevent scrollbars from showing up. Each area should have its own scrollbar.
            }}
        >


            {/* Top app bar */}
            <PatrolAppBar currentTabs={
                this.state.currentTabs
            }
            currentTabIndex={
                this.state.currentTabIndex
            }
            />

            {/* Left feed drawer */}
            <Drawer
                sx={{
                    width: "22rem",
                    flexShrink: 0,
                    "& .MuiDrawer-paper": {
                        width: "22rem",
                        boxSizing: "border-box",
                    },
                }}
                variant="persistent"
                anchor="left"
                open={this.state.feedDrawerOpen}
            >
                <Toolbar />
                <FeedDrawerContent />
            </Drawer>

            {/* Right action drawer */}
            <Drawer
                sx={{
                    width: "25vw",
                    flexShrink: 0,
                    "& .MuiDrawer-paper": {
                        width: "25vw",
                        boxSizing: "border-box",
                    },
                }}
                variant="persistent"
                anchor="right"
                open={this.state.actionDrawerOpen}
            >
                <Toolbar />
                {/* Like for the main content, if something crashes here we don't want to crash the whole app */}
                <ErrorBoundary fallback={<Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    height: "100%",
                }}>
                    <Stack spacing={1} sx={{
                        alignItems: "center",
                    }}>
                        <BrokenImage />
                        <Typography variant="body2">{i18n.t("patrol:home.actionDrawerError")}</Typography>
                    </Stack>
                </Box>} key={this.state.currentTabIndex}>
                    {<RenderTab tab={this.state.currentTabs[this.state.currentTabIndex]} renderType={"sidebar"} />}
                </ErrorBoundary>
            </Drawer>

            {/* Bottom jobs drawer */}
            <Drawer
                sx={{
                    height: this.state.jobsDrawerFooterMode ? "2rem" : "30vh",
                    width: "100vw",
                    flexShrink: 0,
                    "& .MuiDrawer-paper": {
                        height: this.state.jobsDrawerFooterMode ? "2rem" : "30vh",
                        width: "100vw",
                        boxSizing: "border-box",
                    },
                }}
                anchor="bottom"
                variant="persistent"
                open={this.state.jobsDrawerOpen}
            >
                <JobsDrawerContent isInFooterMode={this.state.jobsDrawerFooterMode} />
            </Drawer>

            {/* Main content */}
            <Box sx={{
                // Shrink for feed drawer on the left
                paddingLeft: this.state.feedDrawerOpen ? "22rem" : 0,
                // Shrink for action drawer on the right
                paddingRight: this.state.actionDrawerOpen ? "24vw" : 0,

                overflow: "auto",

                // Height is 100%, or if the jobs drawer is open 100% - 30vh, or if the footer is closed 100% - 2rem
                height: !this.state.jobsDrawerOpen ? "100%" : this.state.jobsDrawerFooterMode ? "calc(100% - 2rem)" : "calc(100% - 30vh)",
                width: "100%",

                position: "absolute",
                top: 0,
                left: 0,
            }}>
                <Toolbar />
                {/*
                We don't want a tab crashing to crash the entire app.
                We render the tab like this so 1. the error boundary is tied to the tab
                and 2. if there is no tab at the current index we just don't render anything.
                 */}
                {this.state.currentTabs[this.state.currentTabIndex] && <ErrorBoundary
                    key={this.state.currentTabIndex}
                    FallbackComponent={
                        (error) => {
                            return <Box sx={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "center",
                                height: "100%",
                            }}>
                                <Alert severity="error">
                                    {i18n.t("patrol:tabContentErrorBoundaryFallback")}
                                    <br />
                                    {error.error.message}
                                </Alert>
                            </Box>;
                        }
                    }>
                    <RenderTab tab={this.state.currentTabs[this.state.currentTabIndex]} />
                </ErrorBoundary>}
            </Box>

            {/* Supasearch */}
            <SupaSearch open={this.state.supaSearchOpen} onClose={() => {
                this.setState({
                    supaSearchOpen: false,
                });
            }} />
        </Box>;
    }
}