/* eslint-disable no-unused-vars */
/*
Component that renders a tab with the required props and also provides
validation for erros and other issues
*/

import { Stack, Typography, Button } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { LocalisedButtons } from "../../../LocalisedButtons";
import { ContentTab, TeyoraPatrol } from "../TeyoraPatrol";
import { TPTab } from "./Tab";

export type TabElement = (tabmeta: {
    tabID: string;
    // eslint-disable-next-line no-unused-vars
    setTPTab: (tab: TPTab<any, any, any>) => void;
}) => React.ReactNode;

interface RenderTabProps {
    tab: ContentTab,
    renderType?: "content" | "sidebar" | "topControls",
}

export default function RenderTab(props: RenderTabProps) {
    const { tab, renderType } = props;
    // Create the tab
    const tabContent = (({
        "content" : tab.content,
        "sidebar" : tab.actionDrawerContent || (() => { return <DefaultEmptySidebar />; }),
        "topControls": tab.topControls || (() => { return null; }) as TabElement,
    })[renderType] || tab.content)({
        tabID: tab.id + (props.renderType ? "-" + props.renderType : ""),
        setTPTab: (cTab: TPTab<any, any, any>) => {
            TeyoraPatrol.TP.__tabPStateDisposals[tab.id] = cTab.__disposePState;
        }
    });

    // Render the tab
    return <>
        {tabContent}
    </>;
}

// Function that creates a tabID (random 6 character string)
export function createTabID() {
    return Math.random().toString(36).substring(2, 8);
}

// Default for empty sidebar render
export function DefaultEmptySidebar() {
    const { t } = useTranslation();
    const lB = LocalisedButtons();
    
    return <Box sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
    }}>
        <Stack spacing={1} sx={{
            alignItems: "center",
        }}>
            <Typography
                color={"text.secondary"}
                textAlign={"center"}

            >
                {t("patrol:noActionDrawerContent")}
            </Typography>
            {/* Close button for convenience */}
            <Button variant="outlined" sx={{
                width: `${lB.CANCEL.length * 1.5}rem`,
            }}
            onClick={()=>{
                TeyoraPatrol.TP.toggleActionDrawer();
            }}
            >{lB.CLOSE}</Button>
        </Stack>
    </Box>;
}