// MediaWiki supasearch results content
// This renders a namespace accordian (we need to track this state independently of the parent)
// and the results inside it

import { AutoAwesome, Book, ContactPage, ExpandMore, Forum, Image, Mms, Public, QuestionAnswer, ThreeP } from "@mui/icons-material";
import { Accordion, AccordionSummary, Typography, AccordionDetails, List, ListItem, ListItemAvatar, ListItemText, Stack, ListItemButton } from "@mui/material";
import { Box } from "@mui/system";
import { DateTime } from "luxon";
import * as React from "react";
import { useTranslation } from "react-i18next";
import Teyora from "../../../../App";
import { MediawikiSearchResult } from "./handlers/MediaWiki";

export default function MWResultContent(props: { wikiID: string, ns: string, i: number, results: MediawikiSearchResult[], onSelect: (result: MediawikiSearchResult) => void }) {
    const { wikiID, ns, i, results } = props;
    const { t } = useTranslation();

    const wikiNSInfo = Teyora.TY.WorkspaceManager.namespaceInfo[wikiID];
    const nsInfo = wikiNSInfo[ns.toString()];

    // Record expanded state for this namespace
    const [expanded, setExpanded] = React.useState(i === 0); // Always expand the first index

    // Determine an icon for this result based on the prefix
    // (should be the same across all wikis but we can default if not)
    let icon = <AutoAwesome />; // Default
    switch (nsInfo.prefix.toLowerCase()) {
        case "":
            // Mainspace
            icon = <Book />;
            break;
        case "talk":
            // Talk namespace
            icon = <QuestionAnswer />;
            break;
        case "user":
            // User namespace
            icon = <ContactPage />;
            break;
        case "user talk":
            // User talk namespace
            icon = <ThreeP />;
            break;
        case "file":
            icon = <Image />;
            break;
        case "file talk":
            icon = <Mms />;
            break;
        case "project":
            icon = <Public />;
            break;
        case "project talk":
            // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
            icon = <Forum />;
            break;
    }

    return (
        <Box>
            {/* The first item is always expanded */}
            <Accordion
                TransitionProps={{ unmountOnExit: true }} // Stop performance from going to hell
                expanded={expanded}
                onChange={(event, expanded) => {
                    setExpanded(expanded);
                } }>
                <AccordionSummary expandIcon={<ExpandMore />}
                    id={`${wikiID}-${ns}-more`}>
                    <Typography variant="body2">{nsInfo.title}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <List>
                        {results.map((result) => {
                            return (
                                <ListItemButton key={result.pID} onClick={()=>{
                                    props.onSelect(result);
                                }}>
                                    <ListItemAvatar>
                                        {icon}
                                    </ListItemAvatar>
                                    <Stack>
                                        <Typography variant="body2">{result.page}</Typography>
                                        {/* Timestamp */}
                                        <Typography variant="body2" sx={{
                                            color: "text.secondary",
                                            fontSize: "0.6rem",
                                            marginTop: "0.2rem",
                                        }}>{DateTime.fromISO(result.timestamp).toLocaleString(DateTime.DATETIME_FULL_WITH_SECONDS)}</Typography>

                                        {/* Snippet */}
                                        <Typography variant="body2" sx={{
                                            color: "text.secondary",
                                            fontStyle: "italic",
                                            fontSize: "0.8rem",
                                        }}>
                                            {/* Render the snippet */}
                                            <span dangerouslySetInnerHTML={{__html: result.snippet}}></span>...
                                        </Typography>
                                        {/* TODO later: Add page action buttons here */}
                                    </Stack>
                                </ListItemButton>
                            );
                        })}
                    </List>
                </AccordionDetails>
            </Accordion>
        </Box>
    );
}