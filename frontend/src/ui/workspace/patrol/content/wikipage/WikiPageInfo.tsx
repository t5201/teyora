/*
The actions sidebar content for any Wiki page.

NOTE: This is a tab element, *DO NOT* use React's state as it will be temporary and disposed when the user exits the tab
*/

import { Stack, CircularProgress } from "@mui/material";
import { Box, Container } from "@mui/system";
import * as React from "react";
import { withTranslation } from "react-i18next";
import { RequiredTabProps, TPTab } from "../../baseclasses/Tab";
import PageInfoCard from "./PageInfoCard";

interface WikiPageInfoSidebarProps extends RequiredTabProps {
    pageTitle: string;
    wikiID: string;
    revisionID?: string;
}

interface WikiPageInfoSidebarPState {
    isLoading: boolean;
}

class WikiPageInfoSidebar extends TPTab<WikiPageInfoSidebarPState, WikiPageInfoSidebarProps, unknown> {
    constructor(props: WikiPageInfoSidebarProps) {
        super(props, {
            isLoading: true,
        });
    }

    render() {
        // Loading
        // return <Box sx={{
        //     display: "flex",
        //     flexDirection: "column",
        //     alignItems: "center",
        //     justifyContent: "center",
        //     height: "100%",
        // }}>
        //     <Stack spacing={1} sx={{
        //         alignItems: "center",
        //     }}>
        //         <CircularProgress />
        //     </Stack>
        // </Box>;


        return <Box>
            <Stack spacing={1} sx={{
                paddingTop: 1,
            }}>
                <PageInfoCard isLoading={true} />
            </Stack>
        </Box>;
    }
}

export default withTranslation()(WikiPageInfoSidebar);