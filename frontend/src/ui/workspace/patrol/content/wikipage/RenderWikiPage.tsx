/*
Loads and renders a wiki page for reading.
*/
import * as React from "react";
import { withTranslation } from "react-i18next";
import { StateFragment } from "@hookstate/core"; // Persist state across tabs
import { Box, CircularProgress, Stack } from "@mui/material";
import WikiAPI from "../../utils/WikiAPI";
import TeyoraUI from "../../../../TeyoraUI";
import { RequiredTabProps, TPTab } from "../../baseclasses/Tab";
import WikiPageFrame from "./WikiPageFrame";

interface RenderWikiPageProps extends RequiredTabProps {
    pageTitle: string;
    wikiID: string;
}

interface RenderWikiPagePState {
    isLoading: boolean;
    pageHTML: string;
    requestPromise: Promise<void> | null;
}


/*
Set up our states:
isLoading: true if the page is loading
pageHTML: the page content
*/

class RenderWikiPage extends TPTab<RenderWikiPagePState, RenderWikiPageProps, unknown> {

    constructor(props: RenderWikiPageProps) {
        // Set base state
        super(props, {
            isLoading: true,
            pageHTML: "",
            requestPromise: null,
        });
    }

    async loadPage() {
        // Load the page - this is done in a promise
        const isLoading = this.getPState("isLoading");
        const pageHTML = this.getPState("pageHTML");
        const { props } = this;
        const { t } = props;


        // Set the loading state to true
        isLoading.set(true);
        try {
            // Load the page
            const pageHTMLs = await WikiAPI.getPageHTML(props.wikiID, props.pageTitle);
            // Set the page HTML
            pageHTML.set(pageHTMLs.text);
            // Set the loading state to false
            isLoading.set(false);
        } catch (error) {
            // Get the error message
            const errorMsg = t("patrol:MWPageLoadError", { "error": error.message });
            // Set the page HTML to the error message
            pageHTML.set(`<span style="color:red">${errorMsg}</span>`);
            // Set the loading state to false
            isLoading.set(false);
            console.error(error);
            // Send a toast
            TeyoraUI.showSnackbar(errorMsg, {
                variant: "error",
            });
        }


    }

    render() {
        const { props } = this;
        const { t } = props;
        const isLoading = this.getPState("isLoading");
        const pageHTMLState = this.getPState("pageHTML");
        const requestPromise = this.getPState("requestPromise");

        // If requestPromise is null, load the page
        if (requestPromise.get() === null) {
            const promise = this.loadPage();
            requestPromise.set(promise);
        }

        // We wrap with a StateFragment to re-render when isLoading changes
        // Otherwise, all other rendering happens in Wikipageframe
        return <StateFragment state={isLoading}>{s =>
            (s.value ? <Box sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                height: "100%",
            }}>
                <Stack spacing={1} sx={{
                    alignItems: "center",
                }}>
                    <CircularProgress />
                </Stack>
            </Box> : <StateFragment state={pageHTMLState}>{s =>
                <WikiPageFrame pageHTML={s.value} />
            }</StateFragment>)}
        </StateFragment>;

 
    }


}
// Every tab needs to be localised
export default withTranslation()(RenderWikiPage);