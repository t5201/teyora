import * as React from "react";
import {TYWordmark, TYWordmarkAuto, TYWordmarkPrimaryContrasted} from "../components/TYLogo";
import {ITYWindowProps} from "../../App";
import Button from "@mui/material/Button";
import "../style/PublicHomePage.css";
import { withTranslation } from "react-i18next";
import { Box, AppBar, Toolbar, Typography, Container, Grid, Card, CardActions, CardContent, IconButton } from "@mui/material";
import Alert from "@mui/material/Alert";
import SelectLangButtonAndDialog from "../dialogs/SelectLangButtonAndDialog";
import TeyoraUI from "../TeyoraUI";
import { LocalisedButtons } from "../LocalisedButtons";
import { KeyboardArrowDown } from "@mui/icons-material";
import NewUserWarningDialog from "./NewUserWarningDialog";
import { Parallax, ParallaxProvider } from "react-scroll-parallax";


/**
 * The PublicHomePage is a Teyora's landing page, featuring a login button that leads to an OAuth verification page,
 * which is used to request access from a Wikipedia user to use their account for Teyora's tools.
 * It also has lots of cool information about Teyora :)
 **/

export interface PublicHomePageProps extends ITYWindowProps {
    loginError?: string;
    hideLoginButton?: boolean;
    loginErrorSeverity?: "error" | "warning" | "info";
}

class PublicHomePage extends React.Component<PublicHomePageProps> {

    state = {
        newUserWarningDialogOpen: false, // Keeps track of whether the new user warning dialog is open
    };

    /**
     * Renders the page.
     **/
    render() : JSX.Element {
        const { t } = this.props; // These are filled in by the withTranslation() decorator.
        const lB = LocalisedButtons();

        // This is where WMF logins happen, including showing the disclaimer is not already shown
        const WMFlogin =  async () : Promise<void> => {
            // Check if the disclaimer has been shown in localStorage
            if (localStorage.getItem("TYWMFLogindisclaimerShown") === null) {
                // If not, show it
                this.setState({newUserWarningDialogOpen: true});
            } else {
                // If it has been shown, just log in
                window.location.href = "/api/oauth/wmf/redirect";
            }
        };

        // Actual rendering

        return (
            <ParallaxProvider>
                <Box
                    className={"TY-login"}
                    sx={{
                        margin: 0,
                        padding: 0,
                        scrollSnapType: "y proximity",
                    }}
                >
                    <AppBar position="sticky" className={"TY-login-header"} color="primary" enableColorOnDark={true}>
                        <Toolbar
                            disableGutters={true}
                            className={"TY-login-header-toolbar"}
                            color="primary">
                            <div className={"TY-login-header-left"}>
                                <TYWordmarkPrimaryContrasted />
                            </div>
                            <div className={"TY-login-header-right"}>
                                <SelectLangButtonAndDialog buttonColor="secondary" />
                            </div>
                        </Toolbar>
                    </AppBar>

                    {/* The top login pane */}
                    <Container className={"TY-login-container"} maxWidth={false} style={{ width: "100%", backgroundImage: "url(/images/backdrops/ColdTower.jpg)", backgroundSize: "cover", backgroundPosition: "center" }}>
                        <Container className={"TY-login-container"} maxWidth="lg" sx={{
                            backgroundImage: "url(/images/backdrops/ColdTower.jpg)",
                            width: "100%",
                        }}>
                            {/* Split container in half */}
                            <Grid container spacing={3} sx={{
                                height: "90vh",
                            }}>
                                {/* Left - logo etc. */}
                                <Grid item xs={12} md={6}>
                                    
                                    {/* Add a box that's centered horizontally and vertically */}
                                    <Box
                                        className={"TY-login-box"}
                                        sx={{
                                            display: "flex",
                                            flexDirection: "column",
                                            justifyContent: "center",
                                            alignItems: "center",
                                            height: "100%",
                                            width: "100%",
                                            padding: "2rem"
                                        }}
                                    >
                                        {/* Add a logo */}
                                        <Parallax speed={-10}>
                                            <TYWordmark />
                                        </Parallax>
                                    </Box> 
                                </Grid>

                                {/* Right - login button etc. */}
                                <Grid item xs={12} md={6} sx={{
                                    display: "flex",
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    height: "100%",
                                    width: "100%",
                                    padding: "2rem"
                                }}>
                                    {/* Add a card that's centered horizontally and vertically */}
                                    <Parallax speed={-1}>
                                        <Card>
                                            <CardContent>
                                                {/* Main content */}

                                                <Typography variant="h6" color="textPrimary">
                                                    {t("login:welcome")}
                                                </Typography>
                                                <br/>
                                                <Typography variant="body1" color="textPrimary">
                                                    {t("login:welcome2")}
                                                </Typography>
                                                <br/>
                                                {/* Log in button redirects to WMF thing, this will generate and store us a JWT */}
                                                <Button 
                                                    variant={"contained"}
                                                    color={"primary"}
                                                    onClick={WMFlogin}
                                                    disabled={this.props.hideLoginButton}
                                                >
                                                    {t("login:serviceProviders.wikimedia")}
                                                </Button>
                                                { this.props.loginError &&
                                        (
                                            <span>
                                                <br/><br/>
                                                <Alert
                                                    severity={this.props.loginErrorSeverity || "error"}
                                                    variant={this.props.loginErrorSeverity == "info" ? "outlined" : "filled"}
                                                >
                                                    {t(this.props.loginError)}
                                                </Alert>
                                            </span>    
                                        )
                                                }
                                                <br/><br/>
                                                <Typography variant="body2" color="textSecondary">
                                                    {t("login:disclaimer")}
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Parallax>

                                    {/* New user warning dialog */}
                                    <NewUserWarningDialog
                                        open={this.state.newUserWarningDialogOpen}
                                        onClose={() => this.setState({newUserWarningDialogOpen: false})}
                                        onLoginButtonClick={() => {
                                            localStorage.setItem("TYWMFLogindisclaimerShown", "true");
                                            WMFlogin();
                                        }}
                                    />

                            
                                </Grid>
                            </Grid>
                            
                        </Container>


                        {/* Spacer with down arrow */}
                        <Container maxWidth="lg"
                            style={{
                                height: "10vh",
                            }}
                        >
                            <Grid container justifyContent = "center">
                                <IconButton aria-label="more" size="large" onClick={()=>TeyoraUI.showAlertBox("Welp, somebody forgot to make this work.")}>
                                    <KeyboardArrowDown />
                                </IconButton>
                            </Grid>
                    
                        </Container>
                    </Container>

                    {/* Next... more information about Teyora */}
                    <Container className={"TY-login-container"} maxWidth={false} sx={{ 
                        width: "100%",
                        backgroundImage: "url(/images/backdrops/Chaos.jpg)",
                        backgroundSize: "cover",
                        backgroundPosition: "center",
                        scrollSnapAlign: "top",
                    }}>
                        <Container className={"TY-login-container"} maxWidth="lg">
                            {/* A cool picture */}
                            <Grid
                                container
                                spacing={2}
                                direction="column"
                                justifyContent="center"
                                style={{ minHeight: "100vh" }}
                            >

                                <h1>Helloooo hii wooo... if this worked.. this is swaggy</h1>
    
                            </Grid> 
                        </Container>
                    </Container>
                
                </Box>
            </ParallaxProvider>
        );
    }
}

export default withTranslation()(PublicHomePage);