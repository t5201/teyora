import { Link, Stack, Typography } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { LocalisedButtons } from "../LocalisedButtons";

interface AttributionProps {
    text: string;
    linkTarget: string;
}

export default function Attribution(props: AttributionProps): JSX.Element {
    const { t } = useTranslation();
    return (
        <Stack spacing={1} direction={"row"}>
            <Typography variant="body2">
                {props.text}
            </Typography>
            <Link
                variant="body2"
                href={props.linkTarget}
                target="_blank"
                rel="noopener noreferrer"
            >
                {t("ui:attribution.linkText")}
            </Link>
        </Stack>
    );
}