/*
static/pageviewerframe/script.mjs
Module that contains patches and scripts for the pageviewerframe.
*/

window.onload = async () => {
    // Select all anchor tags and prevent them from redirecting externally.
    // There's also an extra message in the front end if it happens anyway
    const anchors = document.querySelectorAll('a');
    for (const anchor of anchors) {
        anchor.onclick = (e) => {
            // e.preventDefault();
            // alert(anchor.href);
            // window.location.href = anchor.href;
        }
    }
};